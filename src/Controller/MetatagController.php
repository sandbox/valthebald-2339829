<?php
/**
 * @file
 * Contains \Drupal\metatag\Controller\BookController.
 */

namespace Drupal\metatag\Controller;

use Drupal\book\BookExport;
use Drupal\Component\Utility\String;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller routines for book routes.
 */
class MetatagController extends ControllerBase {

  /**
   * The book manager.
   *
   * @var \Drupal\metatag\BookManagerInterface
   */
  protected $bookManager;

  /**
   * The book export service.
   *
   * @var \Drupal\metatag\BookExport
   */
  protected $bookExport;

  /**
   * Constructs a MetatagController object.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * Returns an administrative overview of all books.
   *
   * @return array
   *   A render array representing the administrative page content.
   *
   */
  function adminOverview() {
    $metatags = metatag_get_info('tags');
    $token_generator = \Drupal::csrfToken();

    $config = \Drupal::config('metatag.config');

    $configs = $config->get();
    ksort($configs);
    //uasort($configs, '_metatag_config_sort');

    $rows = array();
    foreach ($configs as $config) {
      $row = array();

      // Style disabled configurations differently.
      if (!empty($config->disabled)) {
        $row['class'][] = 'disabled';
      }

      $details = '<div class="metatag-config-label collapsed"><a href="#" class="toggle-details">' . String::checkPlain(metatag_config_instance_label($config->instance)) . '</a></div>';
      $details .= '<div class="metatag-config-details js-hide">';

      $inherits = array();
      $parents = metatag_config_get_parent_instances($config->instance);
      array_shift($parents);
      foreach (array_reverse($parents) as $parent) {
        if (!isset($configs[$parent])) {
          $rows[$parent] = array(
            $this->overviewIndent('<div class="metatag-config-label">' . String::checkPlain(metatag_config_instance_label($parent)) . '</div>', $parent),
            '',
          );
        }
        else {
          $inherits[$parent] = metatag_config_instance_label($parent);
          if (!empty($configs[$parent]->disabled)) {
            $inherits[$parent] .= ' ' . t('(disabled)');
          }
        }
      }

      // Show how this config inherits from its parents.
      if (!empty($inherits)) {
        $details .= '<div class="inheritance"><p>' . t('Inherits meta tags from: @parents', array('@parents' => implode(', ', $inherits))) . '</p></div>';
      }

      // Add a summary of the configuration's defaults.
      $summary = array();
      foreach ($config->config as $metatag => $data) {
        // Skip meta tags that were disabled.
        if (empty($metatags[$metatag])) {
          continue;
        }
        $summary[] = array(
          String::checkPlain($metatags[$metatag]['label']) . ':',
          String::checkPlain(metatag_get_value($metatag, $data, array('raw' => TRUE))),
        );
      }
      if (!empty($summary)) {
        $details .= theme('table', array(
          'rows' => $summary,
          'attributes' => array('class' => array('metatag-value-summary')),
        ));
      }
      else {
        $details .= '<p class="warning">No overridden default meta tags</p>';
        $row['class'][] = 'warning';
      }

      // Close the details div
      $details .= '</div>';

      // Add indentation to the leading cell based on how many parents the config has.
      $details = $this->overviewIndent($details, $config->instance);

      $row['data']['details'] = $details;

      $operations = array();
      if (metatag_config_access('disable', $config)) {
        $operations['edit'] = array(
          'title' => ($config->export_type & EXPORT_IN_DATABASE) ? t('Edit') : t('Override'),
          'href' => 'admin/config/search/metatags/config/' . $config->instance,
        );
      }
      if (metatag_config_access('enable', $config)) {
        $operations['enable'] = array(
          'title' => t('Enable'),
          'href' => 'admin/config/search/metatags/config/' . $config->instance . '/enable',
          'query' => array(
              'token' => $token_generator->get('enable-' . $config->instance),
            ) + drupal_get_destination(),
        );
      }
      if (metatag_config_access('disable', $config)) {
        $operations['disable'] = array(
          'title' => t('Disable'),
          'href' => 'admin/config/search/metatags/config/' . $config->instance . '/disable',
          'query' => array(
              'token' => $token_generator->get('disable-' . $config->instance),
            ) + drupal_get_destination(),
        );
      }
      if (metatag_config_access('revert', $config)) {
        $operations['revert'] = array(
          'title' => t('Revert'),
          'href' => 'admin/config/search/metatags/config/' . $config->instance . '/revert',
        );
      }
      if (metatag_config_access('delete', $config)) {
        $operations['delete'] = array(
          'title' => t('Delete'),
          'href' => 'admin/config/search/metatags/config/' . $config->instance . '/delete',
        );
      }
      $operations['export'] = array(
        'title' => t('Export'),
        'href' => 'admin/config/search/metatags/config/' . $config->instance . '/export',
      );
      $row['data']['operations'] = array(
        'data' => array(
          '#theme' => 'links',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );

      $rows[$config->instance] = $row;
    }

    $build['config_table'] = array(
      '#theme' => 'table',
      '#header' => array(
        'type' => t('Type'),
        'operations' => t('Operations'),
      ),
      '#rows' => $rows,
      '#empty' => t('No meta tag defaults available yet.'),
      '#attributes' => array(
        'class' => array('metatag-config-overview'),
      ),
      '#attached' => array(
        'js' => array(
          drupal_get_path('module', 'metatag') . '/metatag.admin.js',
        ),
        'css' => array(
          drupal_get_path('module', 'metatag') . '/metatag.admin.css',
        ),
      ),
    );

    return $build;
  }

  /**
   * Prints a listing of all books.
   *
   * @return array
   *   A render array representing the listing of all books content.
   */
  public function bookRender() {
    $book_list = array();
    foreach ($this->bookManager->getAllBooks() as $book) {
      $book_list[] = $this->l($book['title'], $book['url']);
    }
    return array(
      '#theme' => 'item_list',
      '#items' => $book_list,
      '#cache' => [
        'tags' => \Drupal::entityManager()->getDefinition('node')->getListCacheTags(),
      ],
    );
  }

  /**
   * Generates representations of a book page and its children.
   *
   * The method delegates the generation of output to helper methods. The method
   * name is derived by prepending 'bookExport' to the camelized form of given
   * output type. For example, a type of 'html' results in a call to the method
   * bookExportHtml().
   *
   * @param string $type
   *   A string encoding the type of output requested. The following types are
   *   currently supported in book module:
   *   - html: Printer-friendly HTML.
   *   Other types may be supported in contributed modules.
   * @param \Drupal\node\NodeInterface $node
   *   The node to export.
   *
   * @return array
   *   A render array representing the node and its children in the book
   *   hierarchy in a format determined by the $type parameter.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function bookExport($type, NodeInterface $node) {
    $method = 'bookExport' . Container::camelize($type);

    // @todo Convert the custom export functionality to serializer.
    if (!method_exists($this->bookExport, $method)) {
      drupal_set_message(t('Unknown export format.'));
      throw new NotFoundHttpException();
    }

    $exported_book = $this->bookExport->{$method}($node);
    return new Response(drupal_render($exported_book));
  }

  protected function overviewIndent($text, $instance) {
    $parents = metatag_config_get_parent_instances($instance);
    array_shift($parents);

    // Add indentation to the leading cell.
    if (!empty($parents)) {
      $prefix = array_fill(0, count($parents), '<div class="indent">');
      $suffix = array_fill(0, count($parents), '</div>');
      $text = implode('', $prefix) . $text . implode('', $suffix);
    }

    return $text;
  }
}
